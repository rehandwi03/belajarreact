import React, {Component, useEffect, useState} from 'react';
import {Text, View, Image} from 'react-native';
import gambar from '../../assets/images/gambar.jpeg';

// class FlexBox extends Component {
//   constructor(props) {
//     super(props);
//     console.log('===> constructor');
//     this.state = {
//       subscriber: 200,
//     };
//   }

//   componentDidMount() {
//     console.log('===> componen did mount');
//     setTimeout(() => {
//       this.setState({
//         subscriber: 10,
//       });
//     }, 2000);
//   }

//   componentDidUpdate() {
//     console.log('===> componend did update');
//   }

//   componentWillUnmount() {
//     console.log('===> component Will Unmount');
//   }

//   render() {
//     console.log('===> render');
//     return (
//       <View>
//         <View
//           style={{
//             flexDirection: 'row',
//             backgroundColor: '#c8d6c5',
//             alignItems: 'center',
//             justifyContent: 'space-between',
//           }}>
//           <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#feca57', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#1dd1a1', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#5f27cd', width: 50, height: 50}} />
//         </View>
//         <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//           <Text> Beranda </Text>
//           <Text> Video </Text>
//           <Text> Playlist </Text>
//           <Text> Komunitas </Text>
//           <Text> Channel </Text>
//           <Text> Tentang </Text>
//         </View>

//         <View
//           style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
//           <Image
//             source={gambar}
//             style={{
//               width: 100,
//               height: 100,
//               borderRadius: 100 / 2,
//               marginRight: 14,
//             }}
//           />
//           <View>
//             <Text style={{fontSize: 20, fontWeight: 'bold'}}>Rehan Dwi </Text>
//             <Text>{this.state.subscriber} ribu subscriber </Text>
//           </View>
//         </View>
//         {/*
//         <View style={{flexDirection: 'row', alignItems: 'center'}}>
//           <View style={{backgroundColor: '#feca57', flex: 1, height: 100}} />
//           <View style={{backgroundColor: '#ee5253', flex: 1, height: 100}} />
//           <View style={{backgroundColor: '#1dd1a1', flex: 1, height: 100}} />
//           <View style={{backgroundColor: '#5f27cd', flex: 1, height: 100}} />
//         </View> */}
//       </View>
//     );
//   }
// }

const FlexBox = () => {
  const [subscriber, setSubscriber] = useState(200);

  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setSubscriber(400);
    }, 2000);
    return () => {
      console.log('did update');
    };
  }, [subscriber]);

  // useEffect(() => {
  //   console.log('did update');
  // }, []);

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#c8d6c5',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
        <View style={{backgroundColor: '#feca57', width: 50, height: 50}} />
        <View style={{backgroundColor: '#1dd1a1', width: 50, height: 50}} />
        <View style={{backgroundColor: '#5f27cd', width: 50, height: 50}} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Text> Beranda </Text>
        <Text> Video </Text>
        <Text> Playlist </Text>
        <Text> Komunitas </Text>
        <Text> Channel </Text>
        <Text> Tentang </Text>
      </View>

      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
        <Image
          source={gambar}
          style={{
            width: 100,
            height: 100,
            borderRadius: 100 / 2,
            marginRight: 14,
          }}
        />
        <View>
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>Rehan Dwi </Text>
          <Text>{subscriber} ribu subscriber </Text>
        </View>
      </View>
      {/* 
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{backgroundColor: '#feca57', flex: 1, height: 100}} />
          <View style={{backgroundColor: '#ee5253', flex: 1, height: 100}} />
          <View style={{backgroundColor: '#1dd1a1', flex: 1, height: 100}} />
          <View style={{backgroundColor: '#5f27cd', flex: 1, height: 100}} />
        </View> */}
    </View>
  );
};

export default FlexBox;
