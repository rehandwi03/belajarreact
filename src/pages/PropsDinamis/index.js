import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';

const Story = (props) => {
  return (
    <View style={{alignItems: 'center', marginRight: 20}}>
      <Image
        source={{uri: props.gambar}}
        style={{height: 70, width: 70, borderRadius: 70 / 2}}
      />
      <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.judul}</Text>
    </View>
  );
};

const PropsDinamis = () => {
  return (
    <View>
      <Text>Materi Component Dinamis Props</Text>
      <ScrollView horizontal>
        <View style={{flexDirection: 'row'}}>
          <Story
            judul="Youtube Channel"
            gambar="https://placeimg.com/640/480/people"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
          <Story
            judul="Kelas Online"
            gambar="https://placeimg.com/640/480/nature"
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default PropsDinamis;
