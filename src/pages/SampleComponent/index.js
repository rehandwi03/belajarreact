import React, {Component} from 'react';
import {View, Text, TextInput, Image} from 'react-native';

const SampleComponent = () => {
  return (
    <View>
      <View style={{width: 80, height: 80, backgroundColor: 'red'}}></View>
      <Text> Rehan </Text>
      <Gambar />
      <TextInput style={{borderWidth: 1}} />
      <BoxGreen />
      <Profile />
    </View>
  );
};

const Gambar = () => {
  return (
    <Image
      source={{uri: 'https://placeimg.com/640/480/people'}}
      style={{height: 100, width: 100}}
    />
  );
};

class BoxGreen extends Component {
  render() {
    return <Text>Ini halaman class component</Text>;
  }
}

class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          source={{uri: 'https://placeimg.com/640/480/animals'}}
          style={{height: 100, width: 100}}
        />
        <Text>Ini gambar </Text>
      </View>
    );
  }
}

export default SampleComponent;
